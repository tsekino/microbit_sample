input.onButtonPressed(Button.B, () => {
    basic.showNumber(num)
    basic.pause(700)
    basic.clearScreen()
})
input.onButtonPressed(Button.A, () => {
    basic.showNumber(num)
    music.beginMelody(music.builtInMelody(num), MelodyOptions.Once)
    basic.clearScreen()
})
input.onGesture(Gesture.Shake, () => {
    music.beginMelody(music.builtInMelody(Melodies.BaDing), MelodyOptions.Once)
    if (num < 5) {
        num += 1
    } else {
        num = 0
    }
    basic.showNumber(num)
    basic.pause(400)
    basic.clearScreen()
})
let num = 0
