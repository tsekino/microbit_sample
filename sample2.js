let received = 0
let stone = 0
let result = 0
let scissor = 0
let paper = 0
let push = 0
input.onButtonPressed(Button.B, () => {
    radio.sendValue("val", paper)
    push = paper
    判定()
})
input.onButtonPressed(Button.AB, () => {
    radio.sendValue("val", scissor)
    push = scissor
    判定()
})
function 判定() {
    if (received >= 0 && push >= 0) {
        result = received - push
        if (result == 1) {
            basic.showLeds(`
                . # # # .
                # . . . #
                # . . . #
                # . . . #
                . # # # .
                `)
        } else if (result == 0) {
            basic.showLeds(`
                . . . . .
                . . # . .
                . # # # .
                . # # # .
                # # # # #
                `)
        } else {
            basic.showIcon(IconNames.No)
        }
        received = -1
        push = -1
        basic.pause(700)
        basic.clearScreen()
    }
}
input.onButtonPressed(Button.A, () => {
    radio.sendValue("val", stone)
    push = stone
    判定()
})
radio.onDataPacketReceived(({ receivedString: val, receivedNumber: value }) => {
    if (value == 0) {
        received = 3
    } else {
        received = value
    }
    判定()
})
push = -1
received = -1
paper = 2
scissor = 1
stone = 0
radio.setGroup(1)
